package com.backend.logica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;



/**
 *
 * @author jhoan
 */

@SpringBootApplication
@EntityScan(basePackages = "com.backend.entidad")
@ComponentScan(basePackages = {"com.backend.controlador"})
public class Main {    
    
    public static void main(String [] args){
        SpringApplication.run(Main.class, args);
    }
    
}
