
package com.backend.controlador;

import com.backend.entidad.Cliente;
import com.backend.repositorios.RepositorioCliente;
import com.mysql.fabric.Response;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan 
 */
@Service
@RestController
@EnableJpaRepositories(basePackages = "com.backend.repositorios")

public class ControladorCliente {
    @Autowired
    RepositorioCliente ClienteRepo;
    
    //retorna a todos los datos de la base de datos
    @RequestMapping(
    value = "cliente/all",
    method = RequestMethod.GET,
    produces = "application/json"
    )
    @CrossOrigin
    public List<Cliente> getAll(){
        List<Cliente> result =(List<Cliente>) ClienteRepo.findAll();
        return result;
    }
    
      
    
    //crear un cliente
    @RequestMapping(
            value="cliente/nuevo",
            method = RequestMethod.POST,
            produces = "aplication/json"
    )
    public Cliente create( @RequestBody Cliente cliente ){
        cliente = ClienteRepo.save(cliente);
        //cliente = ClienteRepo.save(null);
    return cliente;
    }
    
    //actualizar cliente
    @RequestMapping(
            value="cliente/update",
            method = RequestMethod.PUT,
            produces = "aplication/json")
    public Cliente update(@RequestBody Cliente cliente){
        cliente = ClienteRepo.save(cliente);
    return cliente;
    }
    
    @RequestMapping(
            value = "persona",
            method = RequestMethod.GET,
            produces = "application/json")
    public Cliente getCliente(@RequestParam("id") Integer id) {
        Cliente result = ClienteRepo.findOne(id);
        //ClienteRepo.delete(result);
        return result;
    }
    @RequestMapping(
            value="cliente/delete",
            method = RequestMethod.DELETE,
            produces = "aplication/json"
    )
    public void delete(@RequestBody Cliente cliente){
        ClienteRepo.delete(cliente);
    }
    
    
}
